﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class DepartmentsController : ApiController
    {
        public HttpResponseMessage Get()
        {
            string query = @"
                select DepartmentId,DepartmentName from 
                dbo.Departments
                          ";
            DataTable table = new DataTable();
            using (var con = new  SqlConnection(ConfigurationManager.
                ConnectionStrings["EmployeeAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query,con))
           using (var da = new SqlDataAdapter(cmd)) 
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }
            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        public string Post(Departments dep)
        {
            try
            {
                string query = @"
                        insert into dbo.Departments values
                        ('" + dep.DepartmentName + @"')
                            ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["EmployeeAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Added Successfully!!";
            }
            catch(Exception)
            {
                return "Fail to Add!!";
            }
        }

        public string Put(Departments dep)
        {
            try
            {
                string query = @"
                        update dbo.Departments set DepartmentName = 
                        '" + dep.DepartmentName + @"'
                            where DepartmentId = "+dep.DepartmentId+@"
                            ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["EmployeeAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Update Successfully!!";
            }
            catch (Exception)
            {
                return "Fail to Update!!";
            }
        }

        public string Delete(int id)
        {
            try
            {
                string query = @"
                        delete from dbo.Departments 
                            where DepartmentId = " + id + @"
                            ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["EmployeeAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Delete Successfully!!";
            }
            catch (Exception)
            {
                return "Fail to Delete!!";
            }
        }



    }
}
