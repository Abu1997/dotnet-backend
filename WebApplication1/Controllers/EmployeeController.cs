﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web;

namespace WebApplication1.Controllers
{
    public class EmployeeController : ApiController
    {

        public HttpResponseMessage Get()
        {
            string query = @"
                select EmployeeId,EmployeeName,Department, 
                convert(varchar(10),DateofJoining,120) as DateofJoining,
                PhotoFileName
                from 
                dbo.Employee
                          ";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["EmployeeAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }
            return Request.CreateResponse(HttpStatusCode.OK, table);
        }

        public string Post(Employee emp)
        {
            try
            {
                string query = @"
                        insert into dbo.Employee values
                        (
                         '" + emp.EmployeeName + @"'
                         ,'" + emp.Department + @"'
                         ,'" + emp.DateofJoining + @"'
                         ,'" + emp.PhotoFileName + @"'
                         )
                         ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["EmployeeAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Added Successfully!!";
            }
            catch (Exception)
            {
                return "Fail to Add!!";
            }
        }

        public string Put(Employee emp)
        {
            try
            {
                string query = @"
                        update Employee set 
                        EmployeeName = '" + emp.EmployeeName + @"'
                        ,Department = '" + emp.Department + @"'
                        ,DateofJoining = '" + emp.DateofJoining + @"'
                        ,PhotoFileName = '" + emp.PhotoFileName + @"'
                            where EmployeeId = " + emp.EmployeeId + @"
                            ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["EmployeeAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Update Successfully!!";
            }
            catch (Exception)
            {
                return "Fail to Update!!";
            }
        }

        public string Delete(int id)
        {
            try
            {
                string query = @"
                        delete from Employee 
                            where EmployeeId = " + id + @"
                            ";
                DataTable table = new DataTable();
                using (var con = new SqlConnection(ConfigurationManager.
                    ConnectionStrings["EmployeeAppDB"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }

                return "Delete Successfully!!";
            }
            catch (Exception)
            {
                return "Fail to Delete!!";
            }

        }


        [Route("api/Employee/GetAllDepartmentNames")]
        [HttpGet]
        public HttpResponseMessage GetAllDepartmentNames()
        {

            string query = @"
                        select DepartmentName from dbo.Departments";
            DataTable table = new DataTable();
            using (var con = new SqlConnection(ConfigurationManager.
                ConnectionStrings["EmployeeAppDB"].ConnectionString))
            using (var cmd = new SqlCommand(query, con))
            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandType = CommandType.Text;
                da.Fill(table);
            }
            return Request.CreateResponse(HttpStatusCode.OK, table);
        }


        [Route("api/Employee/SaveFile")]
        public string SaveFile()
        {
            try
            {
                var HttpRequest = HttpContext.Current.Request;
                var PostedFile = HttpRequest.Files[0];
                string filename = PostedFile.FileName;
                var physicalPath = HttpContext.Current.Server.MapPath("~/Photos" + filename);

                PostedFile.SaveAs(physicalPath);


                return filename;
            }
            catch (Exception)
            {
                return "anonymous.png";
            }
        }
        
    }
}
